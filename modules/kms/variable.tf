

variable "aws_region" {
    default = ""
}

variable key_spec {
  default = ""
}

variable "project_name" {
  
}
variable enabled {}

variable rotation_enabled {
  default = true
}

variable tags {}

variable alias {}

variable policy {}