
output "kms-id" {
    value = aws_kms_key.this.id
}

output "project_name" {
  value = var.project_name
}

# output "alias-name" {
#     value = aws_kms_key.this.id
# }