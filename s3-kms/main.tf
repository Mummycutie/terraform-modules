


aws_region = "us-east-1"

key_spec = "SYMMETRIC_DEFAULT"

enabled  = true


module "s3" {
  source = "../AWS-KMS/example/"

  description = "KMS key for System Manager"
  alias       = "aws-sm-s3"
  policy      = data.aws_iam_policy_document.s3_key.json

  tags = {
    Name  = "s3_key"
    Owner = "wsarwari"
  }
}